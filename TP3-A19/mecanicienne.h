#ifndef __MECANICIENNE__
#define __MECANICIENNE__

#include <list>
#include <iostream>

#include "voiture.h"

class Mecanicienne {
	private:
		int matricule;
        std::list<Voiture*> voitures_assignees;
	public:
        Mecanicienne(int matricule);
        void assigneeSur(Voiture* une_voiture);
        void aTermineAvec(Voiture* une_voiture);
        int getMatricule();
        void afficheAssignations();
};

#endif
